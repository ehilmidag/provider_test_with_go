module example.com/goprovider

go 1.17

require (
	github.com/gofiber/fiber/v2 v2.20.2
	github.com/pact-foundation/pact-go v1.6.4
)

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/hashicorp/go-version v1.0.0 // indirect
	github.com/hashicorp/logutils v0.0.0-20150609070431-0dc08b1671f3 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.31.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20211020174200-9d6173849985 // indirect
)
