package main

import (
	"github.com/gofiber/fiber/v2"
)

type Model struct {
	ID   int    `json:"id"`
	Note string `json:"note"`
}

func main() {
	app := fiber.New()
	app = EndPoints(app)
	app.Listen(":8080")
}

func EndPoints(app *fiber.App) *fiber.App {
	app.Get("/", GetHandler)
	return app
}
func GetHandler(c *fiber.Ctx) error {
	c.Status(fiber.StatusOK)
	c.JSON(Model{ID: 1, Note: "lorem ipsum"})
	return nil
}
