package main

import (
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
)

func Test_Provider(t *testing.T) {
	pact := &dsl.Pact{
		Provider: "noteApp backend",
	}

	pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://localhost:8081",
		BrokerURL:                  "https://ehilmi.pactflow.io",
		BrokerToken:                "ywTPDONGgEOCSUSuTSoJiw",
		PublishVerificationResults: true,
		ProviderVersion:            "1-0-0 aa-1-aa1",
		BeforeEach: func() error {
			app := fiber.New()
			app = EndPoints(app)
			go app.Listen(":8081")
			return nil
		},
		StateHandlers: types.StateHandlers{
			"if i send  get request to /": func() error {
				return nil
			},
		},
	})
}
